require("dotenv").config();
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();
const logger = require("./logger");
var requestcount = 0;

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  logger.info("MySQL Database connected!");
  // try {
  //   await connect();
  //   console.log("Database connected!");
  // } catch (e) {
  //   console.log(e);
  // }
};

const queryProductById = async (productId) => {
  return (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
};

const queryRandomProduct = async () => {
  // use RAND() to choose a random entry and limit it to 1
  return (await mysql.query(`SELECT * 
                              FROM products 
                              ORDER BY RAND() 
                              LIMIT 1;`))[0][0];
};

const queryAllProducts = async () => {
  // select all entries of products
  return (await mysql.query("SELECT * FROM products;"))[0];
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users"))[0];
};

const insertOrder = async (order) => {
  const { userId, totalAmount } = order;
  
  // create new ID and insert in orders
  await mysql.execute(
    "INSERT INTO orders (id, user_id, total_amount) VALUES (?, ?, ?);", 
    [uuid.v4(), userId, totalAmount]
  );
};

const updateUser = async (id, updates) => {
  // with fields from updates, create an update command and execute
  const response = await mysql.query(`UPDATE users SET name = "${updates.name}", 
                                  email = "${updates.email}", 
                                  password = "${updates.password}"
                                  WHERE id = "${id}";`);

  return response[0];
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
