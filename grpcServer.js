const db = process.argv[3];
const PROTO_PATH = "./app.proto";
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const logger = require("./logger");
const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");
const { uuid } = require("uuidv4");

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const app = grpc.loadPackageDefinition(packageDefinition).App;
init();

async function randomProduct(call, callback) {
  const randProd = await queryRandomProduct();
  callback(null, randProd);
}

async function allProducts(call, callback) {
  const products = await queryAllProducts();
  callback(null, { products });
}

async function product(call, callback) {
  const { product_id } = call.request;
  const product = await queryProductById(product_id);
  callback(null, product);
}

async function categories(call, callback) {
  const categories = await queryAllCategories();
  callback(null, { categories });
}

async function allOrders(call, callback) {
  const orders = await queryAllOrders();
  callback(null, { orders });
}

async function ordersByUser(call, callback) {
  const { id } = call.request;
  const orders = await queryOrdersByUser(id);
  callback(null, { orders });
}

async function order(call, callback) {
  const { order_id } = call.request;
  const order = await queryOrderById(order_id);
  callback(null, order);
}

async function user(call, callback) {
  const { id } = call.request;
  const user = await queryUserById(id);
  callback(null, user);
}

async function users(call, callback) {
  const users = await queryAllUsers();
  callback(null, { users });
}

/// TODO: Implement postOrder here (completed)
async function postOrder(call, callback) {
  const orderData = call.request;

  try {
    if (!orderData.user_id || !orderData.total_amount) throw new Error("Missing fields.");

    const order = {
      userId: orderData.user_id,
      totalAmount: orderData.total_amount,
    };

    await insertOrder(order);
    callback(null, order);
  } 
  catch (error) {
    callback({
      code: grpc.status.INTERNAL,
      details: "Failed to post order: " + error.message
    });
  }
}

async function accountDetails(call, callback) {
  console.log(call);
  const { id } = call.request;
  delete call.request.id;
  console.log(call.request);
  const user = await updateUser(id, call.request);
  callback(null, user);
}

/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */

const server = new grpc.Server();
server.addService(app.service, {
  getRandomProduct: randomProduct,
  getAllProducts: allProducts,
  getProduct: product,
  getAllCategories: categories,
  getAllOrders: allOrders,
  getAllUserOrders: ordersByUser,
  getOrder: order,
  getUser: user,
  getAllUsers: users,
  postOrder: postOrder, ///TODO: add postOrder: (Completed)
  patchAccountDetails: accountDetails,
});

server.bindAsync(
  "0.0.0.0:3001",
  grpc.ServerCredentials.createInsecure(),
  () => {
    logger.info("App ready and listening on port 3001");
    server.start();
  },
);
