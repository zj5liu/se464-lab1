const logger = require("./logger");
const db = process.argv[3];
var requestcount = 0;

const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");

const getRandomProduct = async (req, res) => {
  logger.info(`Request ${requestcount} to getRandomProduct: ${req.params}`);
  requestcount += 1;
  const randProd = await queryRandomProduct();
  res.send(randProd);
};

const getProduct = async (req, res) => {
  logger.info(`Request ${requestcount} to getProduct: ${req.params}`);
  requestcount += 1;
  const { productId } = req.params;
  const product = await queryProductById(productId);
  res.send(product);
};

const getProducts = async (req, res) => {
  logger.info(`Request ${requestcount} to getProducts: ${req.params}`);
  requestcount += 1;
  const { category } = req.query;
  const products = await queryAllProducts(category);
  res.send(products);
};

const getCategories = async (req, res) => {
  logger.info(`Request ${requestcount} to getCategories: ${req.params}`);
  requestcount += 1;
  const categories = await queryAllCategories();
  res.send(categories);
};

const getAllOrders = async (req, res) => {
  logger.info(`Request ${requestcount} to getAllOrders: ${req.params}`);
  requestcount += 1;
  const orders = await queryAllOrders();
  res.send(orders);
};

const getOrdersByUser = async (req, res) => {
  logger.info(`Request ${requestcount} to getOrdersByUser: ${req.params}`);
  requestcount += 1;
  const { userId } = req.query;
  const orders = await queryOrdersByUser(userId);
  res.send(orders);
};

const getOrder = async (req, res) => {
  logger.info(`Request ${requestcount} to getOrder: ${req.params}`);
  requestcount += 1;
  const { orderId } = req.params;
  const order = await queryOrderById(orderId);
  res.send(order);
};

const getUser = async (req, res) => {
  logger.info(`Request ${requestcount} to getUser: ${req.params}`);
  requestcount += 1;
  const { userId } = req.params;
  const user = await queryUserById(userId);
  res.send(user);
};

const getUsers = async (req, res) => {
  logger.info(`Request ${requestcount} to getUsers: ${req.params}`);
  requestcount += 1;
  const users = await queryAllUsers();
  res.send(users);
};

///TODO: Implement controller for POST /orders here (COMPLETED)
const postOrder = async (req, res) => {
  logger.info(`Request ${requestcount} to postOrder: ${req.params}`);
  requestcount += 1;
  const orderData = req.body;

  const order = {
    userId: orderData.user_id,
    totalAmount: orderData.total_amount,
  };
  
  const response = await insertOrder(order);
  res.send(response);
}

const patchUser = async (req, res) => {
  logger.info(`Request ${requestcount} to patchUser: ${req.params}`);
  requestcount += 1;
  const updates = req.body;
  const { userId } = req.params;
  const response = await updateUser(userId, updates);
  res.send(response);
};

module.exports = {
  init,
  getProduct,
  getRandomProduct,
  getCategories,
  getAllOrders,
  getOrdersByUser,
  getOrder,
  getProducts,
  getUser,
  getUsers,
  postOrder, ///TODO: Export controller for POST /orders (COMPLETED)
  patchUser,
};
