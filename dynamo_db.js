require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");
const logger = require("./logger");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  logger.info("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  ///TODO: IMPLEMENT THIS (COMPLETED)
  try {
    // select all products from the Products table
    random_product_command = new ScanCommand({
      TableName: "Products"
    });
    response = await docClient.send(random_product_command);
    products_items = response.Items;

    // generate random index and return object at that index
    index = Math.floor(Math.random() * products_items.length);
    return products_items[index];
  } catch(error) {
    console.log(error);
  }
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  ///TODO: Implement this (COMPLETED)
  try {
    
    const all_products_command = new ScanCommand({
      TableName: "Products"
    });

    // if category is specified, filter for category
    if(category !== "") {
      all_products_command = new ScanCommand({
        TableName: "Products",
        FilterExpression: "category_id = :category",
        ExpressionAttributeValues: {
          ":category": category,
        }
      });
    }

    // return items in products of category
    const response = await docClient.send(all_products_command);
    return response.Items;
  } catch(error) {
    console.log(error);
  }
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  ///TODO: Implement this (COMPLETED)

  // create new ID and insert in orders
  const insert_order_command = new PutCommand({
    TableName: "Orders",
    Item: {
      id: uuid.v4(),
      user_id: order.userId,
      total_amount: order.totalAmount
    },
  });
  const response = await docClient.send(insert_order_command);
  return response;
};

const updateUser = async (id, updates) => {
  ///TODO: Implement this (COMPLETED)

  // with fields from updates, create an update command and execute
  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id
    },
    UpdateExpression: "set email = :email, #name_field = :users_name, password = :password", 
    ExpressionAttributeValues: {
      ":email": updates.email || "", 
      ":users_name": updates.name || "", 
      ":password": updates.password || "", 
    },
    ExpressionAttributeNames: {
      "#name_field": "name" 
    },
    ReturnValues: "ALL_NEW", 
  });

  const response = await docClient.send(command);
  return response.Attributes;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
